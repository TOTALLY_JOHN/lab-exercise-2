#include <iostream>
#include <math.h>
using namespace std;

// FUNCTIONS

// MENU 1: Convert Celsius to Fahrenheit.
// COMPLETE
double celsiusToFahrenheit(double c)
{
	return (c * 9 / 5) + 32;
}

// MENU 2: Convert Fahrenheit to Celsius.
// COMPLETE
double fahrenheitToCelcius(double f)
{
    return (f - 32) * 5/9;
}

// MENU 3: Input Radius to get the Circumference of a Circle.
//COMPLETE
double circumferenceOfACircle(double radius)
{
    return 2 * radius * 3.14;
}

//MENU 4: Input Radius to get the Area of a Circle.
//COMPLETE
double areaOfACircle(double radius)
{
    return radius * radius * 3.14;
}

// MENU 5: Input length and width to get the area of rectangle
double areaOfRectangle(double length, double width)
{
    return length * width;
}

// MENU 6: Input three sides' lengths to get area of triangle with Heron's Formula
double areaOfTriangle(double a, double b, double c)
{
    double s, area;
    s = (a + b + c)/2;
    area = sqrt(s * (s-a) * (s-b) * (s-c));
    return area;
}

// MENU 7: Volume of Cylinder
double volumeOfCylinder(double radius, double height)
{
    return radius * radius * height * 3.14;
}

// MENU 8: Volume of Cone
double volumeOfCone(double radius, double height)
{
    return radius * radius * height * 3.14 / 3;
}

int main()
{
	// DECLARE DOUBLE-TYPE'S VARIABLES FOR CELSIUS AND FAHRENHEIT VALUE
	double celsius, fahrenheit, radius, result, length, width, first, second, third, height;
	// DECLARE OF OPTION
	int option;

	//return 0;

	// MENU DRIVEN INTERFACE
	do {
        cout << endl;
        cout << "---------------MENU---------------" << endl;
        cout << endl;
        cout << "1. Convert Celsius to Fahrenheit. " << endl;
        cout << "2. Convert Fahrenheit to Celsius. " << endl;
        cout << "3. Calculate Circumference of a circle. " << endl;
        cout << "4. Calculate Area of a circle. " << endl;
        cout << "5. Area of Rectangle. " << endl;
        cout << "6. Area of Triangle (Heron's Formula). " << endl;
        cout << "7. Volume of Cylinder. " << endl;
        cout << "8. Volume of Cone. " << endl;
        cout << "9. Quit program. " << endl;

        cout << "\n Enter Option: ";
        cin >> option;

        switch(option) {

            // USER SELECTION CASE 1 TO INPUT CELCIUS AND GET FAHRENHEIT VALUE.
            // COMPLETE
            case 1: {
                // INPUT USER'S VALUE
                cout << "Input a Celsius value : ";
                cin >> celsius;
                // CALL THE FUNCTION TO CHANGE THE VALUE INTO FAHRENHEIT
                fahrenheit = celsiusToFahrenheit(celsius);

                // DISPLAY
                cout << "Celsius : " << celsius << " --> ";
                cout << "Fahrenheit : " << fahrenheit << endl;

            } break;

            // USER SELECTION CASE 2 TO INPUT FAHRENHEIT AND GET CELCIUS VALUE.
            // COMPLETE
            case 2: {
                // INPUT USER'S VALUE
                cout << "Input a Fahrenheit value : ";
                cin >> fahrenheit;
                // CALL THE FUNCTION TO CHANGE THE VALUE INTO FAHRENHEIT
                celsius = fahrenheitToCelcius(fahrenheit);

                // DISPLAY
                cout << "Fahrenheit : " << fahrenheit << " --> ";
                cout << "Celsius : " << celsius << endl;

            } break;

            // USER SELECTION CASE 3 TO INPUT RADIUS AND GET THE CIRCUMFERENCE OF A CIRCLE
            //COMPLETE
            case 3:
                // USER'S INPUT
                cout << "Input a radius value of circle: ";
                cin >> radius;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(radius <= 1) {
                    cout << "Value is less than one, Input again";
                    cin >> radius;
                }
                // CALL THE FUNCTON TO CALCULATE THE CIRCUMFERENCE
                result = circumferenceOfACircle(radius);

                // DISPLAY
                cout << "The circumference of circle: " << result << endl;
                break;

            //USER SELECTION CASE 4 TO INPUT RADIUS AND GET AREA OF A CIRCLE
            //COMPLETE
            case 4:
                // USER'S INPUT
                cout << "Input a radius value of circle: ";
                cin >> radius;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(radius <= 1) {
                    cout << "Value is less than one, Input again";
                    cin >> radius;
                }
                // CALL THE FUNCTION TO CALCULATE THE AREA OF A CIRCLE
                result = areaOfACircle(radius);

                // DISPLAY
                cout << "The area of circle: " << result << endl;
                break;

            // USER SELECTION CASE 5 TO INPUT LENGTH AND WIDTH VALUES AND GET THE AREA OF RECTANGLE
            case 5:
                // USER'S INPUTS
                cout << "Input length of rectangle: ";
                cin >> length;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(length <= 1) {
                    cout << "Length is less than one, please input again: ";
                    cin >> length;
                }
                cout << "Input width of rectangle: ";
                cin >> width;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(width <= 1) {
                    cout << "Width is less than one, please input again: ";
                    cin >> width;
                }
                // CALL THE FUNCTION TO CALCULATE THE AREA OF RECTANGLE
                result = areaOfRectangle(length,width);

                // DISPLAY
                cout << "The area of rectangle: " << result << endl;
                break;

            // USER SELECTION CASE 6 TO INPUT LENGTH OF THREE SIDES AND GET THE AREA OF TRIANGLE
            case 6:
                // USER'S INPUTS
                cout << "Input the length of first side of the triangle: ";
                cin >> first;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(first <= 1) {
                    cout << "First side is less than one, please input again: ";
                    cin >> first;
                }
                cout << "Input the length of second side of the triangle: ";
                cin >> second;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(second <= 1) {
                    cout << "Second side is less than one, please input again: ";
                    cin >> second;
                }
                cout << "Input the length of third side of the triangle: ";
                cin >> third;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(third <= 1) {
                    cout << "Third side is less than one, please input again: ";
                    cin >> third;
                }
                // CALL THE FUNCTION TO CALCULATE THE AREA OF TRIANGLE
                result = areaOfTriangle(first, second, third);

                // DISPLAY
                cout << "The area of triangle: " << result << endl;
                break;

            // USER SELECTION CASE 7 TO INPUT RADIUS AND HEIGHT AND GET THE VOLUME OF CYLINDER
            // COMPLETE
            case 7:
                // USER'S INPUTS
                cout << "Input the radius of cylinder: ";
                cin >> radius;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(radius <= 1) {
                	cout << "Radius is less than one, please input again: ";
                    cin >> radius;
				}
                cout << "Input the height of cylinder: ";
                cin >> height;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(radius <= 1) {
                	cout << "Height is less than one, please input again: ";
                    cin >> height;
				}
                // CALL THE FUNCTION TO CALCULATE THE VOLUME OF CYLINDER
                result = volumeOfCylinder(radius,height);

                // DISPLAY
                cout << "The volume of cylinder: " << result << endl;
                break;

            // USER SELECTION CASE 8 TO INPUT RADIUS AND HEIGHT AND GET THE VOLUME OF CONE
            // COMPLETE
            case 8:
                // USER'S INPUTS
                cout << "Input the radius of cone: ";
                cin >> radius;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(radius <= 1) {
                	cout << "Radius is less than one, please input again: ";
                    cin >> radius;
				}
                cout << "Input the height of cone: ";
                cin >> height;
                // VALIDATION FOR VALUE IF LESS THAN 1
                while(radius <= 1) {
                	cout << "Height is less than one, please input again: ";
                    cin >> height;
				}
                // CALL THE FUNCTION TO CALCULATE THE VOLUME OF CYLINDER
                result = volumeOfCone(radius,height);

                // DISPLAY
                cout << "The volume of cone: " << result << endl;
                break;

            // USER SELECTION CASE 9 TO END THE PROGRAM
            case 9:
                cout << "Thanks for using the Program " << endl;
                break;

            // USER INPUT WRONG OPTION
            default:
                cout << "Please input the correct option!" << endl;

        }

	} while(option != 9);

	return 0;
}
